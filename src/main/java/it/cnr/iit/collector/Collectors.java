package it.cnr.iit.collector;

import java.util.List;

public class Collectors {

	private List<Collector> collectors;
	private long timeWindow;
	
	public Collectors(){}

	public List<Collector> getCollectors() {
		return collectors;
	}
	public void setCollectors(List<Collector> collectors) {
		this.collectors = collectors;
	}
	public long getTimeWindow() {
		return timeWindow;
	}
	public void setTimeWindow(long timeWindow) {
		this.timeWindow = timeWindow;
	}
}
