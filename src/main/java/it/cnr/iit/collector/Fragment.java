package it.cnr.iit.collector;

public class Fragment {
	
	private String						regex;
	private String						searchKeyword;
	private long							lastLineRead;
	private FragmentMetadata	fragmentMetadata;
	private long							lastUnixTime;
	
	public Fragment() {
	}
	
	public String getRegex() {
		return regex;
	}
	
	public void setRegex(String regex) {
		this.regex = regex;
	}
	
	public String getSearchKeyword() {
		return searchKeyword;
	}
	
	public void setSearchKeyword(String searchKeyword) {
		this.searchKeyword = searchKeyword;
	}
	
	public long getLastLineRead() {
		return lastLineRead;
	}
	
	public void setLastLineRead(long lastLineRead) {
		this.lastLineRead = lastLineRead;
	}
	
	public FragmentMetadata getFragmentMetadata() {
		return fragmentMetadata;
	}
	
	public void setFragmentMetadata(FragmentMetadata fragmentMetadata) {
		this.fragmentMetadata = fragmentMetadata;
	}
	
	public long getLastUnixTime() {
		return lastUnixTime;
	}
	
	public void setLastUnixTime(long lastUnixTime) {
		this.lastUnixTime = lastUnixTime;
	}
	
}
