package it.cnr.iit.collector;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.FileSystemUtils;

import it.cnr.iit.log.utils.LogConfiguration;
import it.cnr.iit.log.utils.LogOperations;

public class MainCollector implements Runnable {
	
	long									startTime					= 0;
	long									endTime						= 0;
	long									currentTime				= 0;
	long									tmpCurrentTime		= 0;
	int										countFragment			= 0;
	
	private static Logger	logger						= LogManager
	    .getLogger(MainCollector.class);
	
	boolean								windowOver				= true;
	ArrayList<String>			stringToWrite			= new ArrayList<String>();
	
	ArrayList<String>			netflowFilesRead	= new ArrayList<String>();;
	
	@Autowired
	LogOperations					logOperations;
	
	@Autowired
	LogConfiguration			logConfiguration;
	
	@Override
	public void run() {
		
		System.out.println("COLLECTOR:> The collector is running!");
		Collectors collectorList = null;
		
		File file;
		String json = "";
		String userHome = System.getProperty("user.home");
		
		String configPath = userHome + "/" + logConfiguration.getConfigPath();
		if (new File(configPath).exists() == false) {
			logOperations.CreateFolder(configPath, "/c3isp");
			logger.error("COLLECTOR:> collector-conf.json file is missing");
			System.exit(1);
		}
		
		try {
			file = new File(
			    configPath + "/" + logConfiguration.getCollectorConfigFile());
			json = new String(Files.readAllBytes(file.toPath()));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if ((collectorList = (Collectors) logOperations.ReadFromJson(json,
		    Collectors.class)) == null) {
			logger.error(
			    "COLLECTOR:> The configuration file collector-conf.json is empty");
			System.exit(1);
		}
		
		while (true) {
			
			for (Collector c : collectorList.getCollectors()) {
				if (c.getDataType().contains("ssh")) {
					logger.info("COLLECTOR:> Analyzing ssh log...");
				}
				
				if (c.getDataType().contains("dns")) {
					logger.info("COLLECTOR:> Analyzing dns log...");
				}
				
				if (c.getDataType().contains("netflow")) {
					logger.info("COLLECTOR:> Analyzing netflow log...");
					
					analyzeNetflowLogs(c);
					tmpCurrentTime = currentTime;
					ResetVariables();
					
					continue;
				}
				
				AnalyzeLog(c);
				tmpCurrentTime = currentTime;
				ResetVariables();
			}
			
			logger.info("COLLECTOR:> Number of fragments: " + countFragment);
			if (countFragment > 0) {
				logOperations.SendFragmentsToAggregator();
				countFragment = 0;
			} else
				logOperations.sendHeartbeat();
			
			logger.info("COLLECTOR:> Deleting fragments...");
			if (!cleanAll()) {
				logger.info("COLLECTOR:> No fragment to delete");
			} else {
				logger.info("COLLECTOR:> Fragments deleted!");
			}
			
			try {
				logger.info("COLLECTOR:> Collector is going to sleep for "
				    + collectorList.getTimeWindow() + " seconds.");
				Thread.sleep(collectorList.getTimeWindow() * 1000);
				currentTime = tmpCurrentTime;
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	public void AnalyzeLog(Collector c) {
		
		try {
			// Reading the log file
			FileInputStream fstream = new FileInputStream(c.getFilePath());
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
			
			// Prepare the proper regex
			Pattern pattern = Pattern.compile(c.getFragment().getRegex());
			Matcher matcher;
			boolean initialize = true;
			
			String strLine;
			int lineCount = 0;
			
			// Read until the end of the file
			while ((strLine = br.readLine()) != null) {
				
				// Read the file starting from the last time
				lineCount++;
				if (lineCount <= c.getFragment().getLastLineRead())
					continue;
				
				matcher = pattern.matcher(strLine);
				// Match the date in a line if present and the keyword to distinguish
				// between many log files
				if (matcher.find()
				    && strLine.contains(c.getFragment().getSearchKeyword())) {
					
					// calculate the current time in secs from the read line
					currentTime = logOperations
					    .FromDate2Sec(logOperations.DateFormatter(c, matcher.group(0)));
					
					// Inizialize startTime and endTime
					if (initialize) {
						if (c.getDataType().contains("netflow")) {
							setNetflowEndStartTime(c);
						} else {
							SetEndStartTime(c);
						}
						initialize = false;
					}
					
					if (currentTime > endTime) {
						// If the time window is over, we can move on
						SetEndStartTime(c);
						// We of course must write the lines read in the fragment
						WriteInFragment(c);
						stringToWrite.clear();
					}
					
					// if the time window is over, we create a new fragment file
					if (windowOver) {
						// create new fragment
						// System.out.println("Creating new "+c.getDataType()+"
						// fragment...");
						setMetadata(c);
						stringToWrite.add(strLine);
						windowOver = false;
						continue;
					}
					
					// We store in the fragment all the log lines found
					stringToWrite.add(strLine);
					
				}
			}
			
			c.getFragment().setLastLineRead(lineCount);
			// if there are lines to write, we must add them in the fragment file
			if (stringToWrite.size() > 0) {
				// System.out.println("Writing last lines...");
				
				if (logOperations.writeFragment(c, stringToWrite) == -1) {
					// System.out.println("Error in writing fragment");
					System.exit(1);
				}
				
				stringToWrite.clear();
			}
			
			fstream.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void analyzeNetflowLogs(Collector c) {
		
		// Reading all the nfcapd files: these raw files need to be converted with
		// nfdump command in order
		// to be read
		
		try (Stream<Path> filePathStream = Files.walk(Paths.get(c.getFilePath()))) {
			filePathStream.forEach(filePath -> {
				if (Files.isRegularFile(filePath)) {
					if (!filePath.toString().contains("current")) {
						
						// We store the filePath value of collector-conf.json in a temp
						// variable.
						// It contains the path to the netflow raw files folder
						String folderPath = c.getFilePath();
						String filename = filePath.toString();
						
						if (!netflowFilesRead.contains(filename)) {
							
							String tmpfilename = folderPath + "tmp.log";
							File tmpfile = new File(tmpfilename);
							
							// a nfcapd will be converted and stored in a tmp.log file
							try {
								// launching a new thread to execute nfdump command
								String[] command = new String[] { "nfdump", "-r", filename,
								    "-o", "line" };
								Process proc = new ProcessBuilder(command)
								    .redirectOutput(tmpfile).start();
								// wait until it finishes
								proc.waitFor();
								if (proc.exitValue() != 0) {
									System.out.println(
									    "COLLECTOR:> Error: there was a problem in executing "
									        + "nfdump command whit the file " + filename);
								}
								
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
							// We set the collector filePath variable with the tmp.log file in
							// order to analyze it
							c.setFilePath(tmpfilename);
							AnalyzeLog(c);
							ResetVariables();
							c.getFragment().setLastLineRead(0);
							// Restoring the filePath variable with the folder containing all
							// the raw netflow logs
							c.setFilePath(folderPath);
							// deleting tmp.log
							tmpfile.delete();
							// add the file read inside the list
							netflowFilesRead.add(filename);
						}
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void setMetadata(Collector c) {
		
		createLogFolders(c);
		c.getFragment().getFragmentMetadata().setStartTime(startTime);
		c.getFragment().getFragmentMetadata().setEndTime(endTime);
		c.getFragment().getFragmentMetadata()
		    .setHostname(logConfiguration.getUsername());
		countFragment++;
	}
	
	public void createLogFolders(Collector c) {
		
		String path = logConfiguration.getLogPath();
		path += "/" + logConfiguration.getUsername();
		path += "/" + c.getDataType();
		logOperations.CreateFolder(path, c.getDataType());
		LogOperations.filePath = path;
	}
	
	public void SetEndStartTime(Collector c) {
		startTime = currentTime;
		endTime = startTime + c.getTimeWindow();
	}
	
	public void setNetflowEndStartTime(Collector c) {
		
		try {
			
			String line = null;
			// Reading the log file
			FileInputStream fstream = new FileInputStream(c.getFilePath());
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
			
			long delta = 0, maxDelta = 0;
			// Retrieve the line which contains the start time in order to set
			// startTime variable
			while ((line = br.readLine()) != null) {
				if (line.contains("Time window")) {
					String array[] = line.split(" ");
					
					// Setting startTime and endTime variables
					startTime = logOperations.FromDate2Sec(
					    logOperations.DateFormatter(c, array[2] + " " + array[3]));
					endTime = startTime + c.getTimeWindow();
				}
				
				// search in the line for possible packet delay
				delta = findNetflowDelay(c, line);
				// search the max packets delay
				maxDelta = findNetflowMaxDelta(delta, maxDelta);
			}
			
			if ((c.getTimeWindow()) < maxDelta) {
				endTime += maxDelta - c.getTimeWindow();
				System.out.println("COLLECTOR:> raising the timeWindow by "
				    + (maxDelta - c.getTimeWindow())
				    + " seconds in order to accomodate packets delay");
				System.out.println("COLLECTOR:> The new endTime is "
				    + logOperations.FromSec2DateString(endTime));
			}
			
			br.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public long findNetflowDelay(Collector c, String line) {
		
		Pattern pattern = Pattern.compile(c.getFragment().getRegex());
		Matcher matcher;
		matcher = pattern.matcher(line);
		
		if (matcher.find()) {
			String array[] = line.split("\\.");
			array = array[1].split(" ");
			return Long.parseLong(array[array.length - 1]);
		}
		
		return 0;
	}
	
	public long findNetflowMaxDelta(long delta1, long delta2) {
		if (delta1 > delta2)
			return delta1;
		else
			return delta2;
	}
	
	public void WriteInFragment(Collector collector) {
		windowOver = true;
		
		// System.out.println("Writing in fragment "+ count);
		if (logOperations.writeFragment(collector, stringToWrite) == -1) {
			// System.out.println("Error in writing fragment");
			System.exit(1);
		}
	}
	
	public void ResetVariables() {
		// System.out.println("Resetting static variables...");
		startTime = 0;
		endTime = 0;
		currentTime = 0;
		windowOver = true;
		stringToWrite.clear();
	}
	
	public boolean cleanAll() {
		return FileSystemUtils
		    .deleteRecursively(new File(logConfiguration.getLogPath()));
	}
	
}
