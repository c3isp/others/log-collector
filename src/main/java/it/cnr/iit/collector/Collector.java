package it.cnr.iit.collector;

public class Collector {

		private String dataType;
		private String filePath;
		private long timeWindow;
		
		private Fragment fragment;
		
		public Collector() {}
		
		public String getDataType() {
			return dataType;
		}
		public void setDataType(String dataType) {
			this.dataType = dataType;
		}
		public String getFilePath() {
			return filePath;
		}
		public void setFilePath(String filePath) {
			this.filePath = filePath;
		}
		public long getTimeWindow() {
			return timeWindow;
		}
		public void setTimeWindow(long timeWindow) {
			this.timeWindow = timeWindow;
		}
		public Fragment getFragment() {
			return fragment;
		}
		public void setFragment(Fragment fragment) {
			this.fragment = fragment;
		}
}
