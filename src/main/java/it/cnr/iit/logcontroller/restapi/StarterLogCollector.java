package it.cnr.iit.logcontroller.restapi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.client.RestTemplate;

import it.cnr.iit.collector.MainCollector;
import it.cnr.iit.log.utils.LogConfiguration;
import it.cnr.iit.log.utils.LogOperations;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.BasicAuth;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class StarterLogCollector extends SpringBootServletInitializer {
	private final static Logger LOGGER = LoggerFactory.getLogger(StarterLogCollector.class);

	private static final String URL_PATH = "/v1/.*";

	@Value("${security.activation.status}")
	private boolean securityActivationStatus;
	@Value("${security.user.name}")
	private String username;
	@Value("${security.user.password}")
	private String password;

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(StarterLogCollector.class);
	}

	@Bean
	public Docket documentation() {
		Docket docket = new Docket(DocumentationType.SWAGGER_2);
		docket.apiInfo(metadata());
		if (!securityActivationStatus) {
			return docket.select().paths(PathSelectors.regex(URL_PATH)).build();
		} else {
			return docket.securitySchemes(new ArrayList<BasicAuth>(Arrays.asList(new BasicAuth("basicAuth"))))
					.securityContexts(new ArrayList<SecurityContext>(Arrays.asList(securityContext()))).select()
					.paths(PathSelectors.regex(URL_PATH)).build();
		}
	}

	private SecurityContext securityContext() {
		return SecurityContext.builder().securityReferences(defaultAuth()).forPaths(PathSelectors.regex(URL_PATH))
				.build();
	}

	List<SecurityReference> defaultAuth() {
		AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
		AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
		authorizationScopes[0] = authorizationScope;
		return new ArrayList<SecurityReference>(Arrays.asList(new SecurityReference("basicAuth", authorizationScopes)));
	}

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
		return restTemplateBuilder.basicAuthentication(username, password).build();
	}

	@Bean
	public LogConfiguration getLogConfiguration() {
		return new LogConfiguration();
	}

	@Bean
	public LogOperations getLogOperations() {
		return new LogOperations();
	}

	@Bean
	public UiConfiguration uiConfig() {
		return new UiConfiguration("*");
	}

	@Bean
	public Executor asyncExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(10);
		executor.setMaxPoolSize(20);
		executor.setQueueCapacity(500);
		executor.setThreadNamePrefix("IAI-API ");
		executor.initialize();
		return executor;
	}

	@Bean
	public MainCollector mainAggregator() {
		return new MainCollector();
	}

	@Autowired
	private MainCollector mainCollector;

	@PostConstruct
	public void startCollector() {
		ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
		executor.schedule(mainCollector, 1, TimeUnit.SECONDS);
	}

	private ApiInfo metadata() {
		return new ApiInfoBuilder().title("Log Controller").description("LogController").version("1.0")
				.contact(new Contact("Calogero Lo Bue", "", "calogero.lobue@iit.cnr.it")).build();
	}

	public static void main(String[] args) {
		SpringApplication.run(StarterLogCollector.class, args);
	}
}