package it.cnr.iit.log.utils;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(ignoreUnknownFields = true, ignoreInvalidFields = true)
public class LogConfiguration {
	
	@Value("${log-controller.collector-config-file}")
	private String	collectorConfigFile;
	@Value("${log-controller.aggregator-config-file}")
	private String	aggregatorConfigFile;
	@Value("${log-controller.api.send-fragment}")
	private String	sendFragment;
	@Value("${log-controller.api.send-heartbeat}")
	private String	sendHeartbeat;
	
	@Value("${log-controller.log-path}")
	private String	logPath;
	@Value("${log-controller.config-path}")
	private String	configPath;
	
	@Value("${log-controller.username}")
	private String	username;
	@Value("${log-controller.home}")
	private String	home;
	
	public LogConfiguration() {
	}
	
	@PostConstruct
	public void init() {
		if (username == null || username.isEmpty())
			username = System.getProperty("user.name");
		if (home == null || home.isEmpty())
			home = System.getProperty("user.home");
	}
	
	public String getCollectorConfigFile() {
		return collectorConfigFile;
	}
	
	public void setCollectorConfigFile(String collectorConfigFile) {
		this.collectorConfigFile = collectorConfigFile;
	}
	
	public String getAggregatorConfigFile() {
		return aggregatorConfigFile;
	}
	
	public void setAggregatorConfigFile(String aggregatorConfigFile) {
		this.aggregatorConfigFile = aggregatorConfigFile;
	}
	
	public String getSendFragment() {
		return sendFragment;
	}
	
	public void setSendFragment(String sendFragment) {
		this.sendFragment = sendFragment;
	}
	
	public String getSendHeartbeat() {
		return sendHeartbeat;
	}
	
	public void setSendHeartbeat(String sendHeartbeat) {
		this.sendHeartbeat = sendHeartbeat;
	}
	
	public String getLogPath() {
		return logPath;
	}
	
	public void setLogPath(String logPath) {
		this.logPath = logPath;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getHome() {
		return home;
	}
	
	public void setHome(String home) {
		this.home = home;
	}
	
	public String getConfigPath() {
		return configPath;
	}
	
	public void setConfigPath(String configPath) {
		this.configPath = configPath;
	}
}
