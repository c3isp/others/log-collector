package it.cnr.iit.log.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import it.cnr.iit.collector.Collector;
import it.cnr.iit.collector.FragmentMetadata;

@Component
public class LogOperations {
	
	@Autowired
	public LogConfiguration	logConfig;
	
	@Autowired
	public RestTemplate			restTemplate;
	
	public LogOperations() {
	}
	
	static public String filePath = "";
	
	public int writeFragment(Collector c, ArrayList<String> stringToWrite) {
		
		try {
			
			FragmentMetadata metadata = c.getFragment().getFragmentMetadata();
			String logName = filePath + "/"
			    + FromSec2DateString(metadata.getStartTime()) + ".fgt";
			
			System.out.println("UTILS:> Writing " + logName + " " + c.getDataType()
			    + " fragment...");
			PrintWriter writer = new PrintWriter(
			    new BufferedWriter(new FileWriter(logName)));
			String firstLine = "#dataType=" + c.getDataType() + "; startTime="
			    + FromSec2DateString(metadata.getStartTime()) + "; endTime="
			    + FromSec2DateString(metadata.getEndTime()) + "; organization="
			    + metadata.getOrganization() + "; hostname=" + logConfig.getUsername()
			    + "; vendor=" + metadata.getVendor();
			
			if (c.getDataType().contains("dns")
			    || c.getDataType().contains("netflow"))
				firstLine += "; severity=" + metadata.getSeverity();
			else if (c.getDataType().contains("ssh")) {
				firstLine += "; year=" + LocalDate.now().getYear();
			}
			
			writer.println(firstLine);
			
			for (String s : stringToWrite) {
				writer.println(s);
			}
			
			writer.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1;
		}
		
		return 0;
	}
	
	public void SendFragmentsToAggregator() {
		
		/*
		 * File folder = new File(LOG_PATH+"/created/");
		 * 
		 * if(!folder.exists()) { System.out.println("No fragment to send yet");
		 * return; }
		 */
		
		System.out.println("called SendFragmentsToAggregator");
		String uri = logConfig.getSendFragment();
		
		MultiValueMap<String, Resource> map = new LinkedMultiValueMap<String, Resource>();
		
		try (Stream<Path> filePathStream = Files
		    .walk(Paths.get(logConfig.getLogPath()))) {
			filePathStream.forEach(filePath -> {
				if (Files.isRegularFile(filePath)) {
					map.add("user-file", new FileSystemResource(filePath.toString()));
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);
		
		HttpEntity<MultiValueMap<String, Resource>> httpEntity = new HttpEntity<MultiValueMap<String, Resource>>(
		    map, httpHeaders);
		
		ResponseEntity<String> response = restTemplate.exchange(uri,
		    HttpMethod.POST, httpEntity, String.class);
		
		System.out.println("UTILS:> " + response.getBody());
	}
	
	public void sendHeartbeat() {
		
		System.out
		    .println("COLLECTOR:> Sending Heartbeat to LogController at time: "
		        + (new Date().toString()));
		HttpEntity<String> request = new HttpEntity<>(
		    "Controller: " + logConfig.getUsername());
		restTemplate.exchange(logConfig.getSendHeartbeat(), HttpMethod.POST,
		    request, String.class);
		
		// RESTUtils.asyncPost(logConfig.getSendHeartbeat(),
		// "Controller: " + logConfig.getUsername());
	}
	
	public <T> T ReadFromJson(String data, Class<T> className) {
		
		try {
			return new ObjectMapper().readValue(data, className);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public String FromSec2DateString(long time) {
		
		Date date = new java.util.Date(time * 1000);
		SimpleDateFormat formatter = new SimpleDateFormat(
		    "yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH);
		String formattedDate = formatter.format(date);
		return formattedDate;
	}
	
	public String DateFormatter(Collector c, String input) {
		
		String formattedDate = null;
		
		try {
			SimpleDateFormat parser = null;
			String year = "";
			
			if (c.getDataType().contains("ssh")) {
				Date date = new Date();
				Calendar calendar = new GregorianCalendar();
				calendar.setTime(date);
				year = String.valueOf(calendar.get(Calendar.YEAR));
				input = year + " " + input;
				parser = new SimpleDateFormat("yyyy MMM d HH:mm:ss", Locale.ENGLISH);
			}
			
			if (c.getDataType().contains("dns")) {
				parser = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss.SSS",
				    Locale.ENGLISH);
			}
			
			if (c.getDataType().contains("netflow")) {
				parser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
			}
			
			Date date = parser.parse(input);
			SimpleDateFormat formatter = new SimpleDateFormat(
			    "yyyy-MM-dd'T'HH:mm:ss.SZ", Locale.ENGLISH);
			formattedDate = formatter.format(date);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return formattedDate;
		
	}
	
	public long FromDate2Sec(String toCovert) {
		
		Date date = null;
		long unixTime = 0;
		try {
			
			SimpleDateFormat parser = new SimpleDateFormat(
			    "yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.ENGLISH);
			date = parser.parse(toCovert);
			unixTime = date.getTime() / 1000;
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return unixTime;
	}
	
	public void CreateFolder(String path, String dir) {
		
		File file = new File(path);
		if (!file.isDirectory()) {
			System.out.println("UTILS:> Creating '" + dir + "' directory...");
			file.mkdirs();
		}
	}
}
